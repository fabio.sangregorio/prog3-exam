object countC7 {
  def main(args: Array[String]) {
    println(countC7for(0, 10))
    println(countC7foreach(0, 10))
    println(countC7map(0, 10))
    println(countC7recInit(0, 10))
    println(countC7fold(0, 10))
    println(countC7filter(0, 10))
    println(countCNfilter(0, 11, 0, contieneN))
  }

  def contieni7(n: Int): Int = if(n.toString().contains("7")) 1 else 0
  
  def countC7for(a: Int, b: Int): Int = {
    var sum = 0
    for(n <- List.range(a, b)) sum += contieni7(n)
    sum
  }
  
  def countC7foreach(a: Int, b: Int): Int = {
    var sum = 0
    List.range(a,b).foreach(n => sum += contieni7(n))
    sum
  }
  
  def countC7recInit(a: Int, b: Int): Int = {
    countC7rec(List.range(a, b))
   
  }

  def countC7rec(lista: List[Int]): Int = {
    if(lista.isEmpty) 0
    else {
      contieni7(lista.head) + countC7rec(lista.takeRight(lista.size - 1))
    }
  }
  
  def countC7map(a: Int, b: Int): Int = {
    List.range(a, b).map(i => contieni7(i)).reduce(_ + _)
  }
  
  def countC7fold(a: Int, b: Int): Int = {
    List.range(a, b).foldLeft(0)((a,b) => a + contieni7(b))
  }
  
  def countC7filter(a: Int, b: Int): Int = {
    List.range(a,b).filter(i => contieni7(i) == 1).size
  }
  
  def contieneN(num: Int, n: Int): Int = if(num.toString().contains(n.toString())) 1 else 0
  
  def countCNfilter(a: Int, b: Int, n: Int, f: (Int, Int) => Int): Int = {
    def count(): Int = {
      List.range(a,b).filter(i => f(i,n) == 1).size
    }
    
    count()
  }
}