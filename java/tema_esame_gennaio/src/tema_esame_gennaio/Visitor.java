package tema_esame_gennaio;

public interface Visitor<T> {
	public T visit(Manager m);
	public T visit(Impiegato i);
}
