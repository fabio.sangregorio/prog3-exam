public class NomeVisitor implements Visitor<String> {
	@Override
	public String visit(Atomico atomico) {
		return atomico.nome;
	}

	@Override
	public String visit(Composto composto) {
		String nome = "[";
		NomeVisitor v = new NomeVisitor();
		for(Componente c: composto.componenti) nome += c.accept(v) + " ";
		return nome + "]";
	}
}