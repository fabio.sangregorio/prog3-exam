public class PrezzoVisitor implements Visitor<Integer> {
	@Override
	public Integer visit(Atomico atomico) {
		return atomico.prezzo;
	}

	@Override
	public Integer visit(Composto composto) {
		return Componente.sommaCosto(composto.componenti);
	}
}