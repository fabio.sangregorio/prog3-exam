public class PerimetroVisitor implements Visitor {
	@Override
	public double visit(RettangoloV rettangoloV) {
		return 2 * (rettangoloV.base + rettangoloV.altezza);
	}

	@Override
	public double visit(CerchioV cerchioV) {
		return 2 * Math.PI * cerchioV.raggio;
	}

	@Override
	public double visit(QuadratoV quadratoV) {
		return 4 * quadratoV.base;
	}
}