/*
 * listacircolareinteri.c
 *
 *  Created on: Jan 28, 2020
 *      Author: fabio
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "listacircolareinteri.h"

typedef struct {
	int max_size;
	int curr_size;
	int position;
	int *arr;
} lista;

lista_ref make(int max_size) {
	lista *list = malloc(sizeof(lista));
	list->arr = malloc(sizeof(int) * max_size);
	list->max_size = max_size;
	list->curr_size = 0;
	list->position = 0;
	return (lista_ref) list;
}

int prossimo(lista_ref list) { // TODO: provare con lista*
	if (((lista*) list)->position + 1 >= ((lista*) list)->curr_size)
		((lista*) list)->position = 0;
	else
		((lista*) list)->position++;
	return ((lista*) list)->arr[((lista*) list)->position];
}

bool cerca(lista_ref list, int n) {
	int curr_pos = ((lista*) list)->position;
	if (prossimo(list) == n)
		return true;
	while (((lista*) list)->position != curr_pos)
		if (prossimo(list) == n)
			return true;
	return false;
}

void aggiungi(lista_ref list, int n) {
	if (((lista*) list)->curr_size == ((lista*) list)->max_size) {
		printf("Impossibile aggiungere alla lista: lista piena.\n");
		return;
	}
	((lista*) list)->arr[((lista*) list)->curr_size] = n;
	((lista*) list)->curr_size++;
}

int nDigits(int i) {
	int n = 1;
	if (i < 0) {
		n++;
		i *= -1;
	}
	while (i > 9) {
		n++;
		i /= 10;
	}
	return n;
}

char* toString(lista_ref list) {
	int curr_pos = ((lista*) list)->position;
	int size = 2 + sizeof(char) * nDigits(prossimo(list));
	while (((lista*) list)->position != curr_pos) {
		size += sizeof(char) * nDigits(prossimo(list));
	}

	char *res = malloc(size);
	res[0] = '\0';
	int i = 0;
	i += sprintf(res, "%d", prossimo(list));

	while (((lista*) list)->position != curr_pos) {
		i += sprintf(res + i, "%d", prossimo(list));
	}

	res[i] = '\0';
	return res;
}

void elimina(lista_ref list) {
	free(((lista*) list)->arr);
	free(list);
}
