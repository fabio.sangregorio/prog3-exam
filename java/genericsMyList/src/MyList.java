import java.util.ArrayList;
import java.util.Collections;

public class MyList<T extends Comparable<? super T>> extends ArrayList<T> {	
	public void insert(T persona) {
		this.add(persona);
	}
	
	public T getGiovanest() {
		return Collections.min(this);
	}	
}