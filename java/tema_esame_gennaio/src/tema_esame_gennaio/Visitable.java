package tema_esame_gennaio;

public interface Visitable {
	public <T> T accept(Visitor<T> v);
}
