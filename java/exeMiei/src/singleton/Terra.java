package singleton;

public class Terra {
	public boolean raining;
	private static Terra instance = null;
	private Terra() {
		raining = true;
	}
	public static Terra getInstance() {
		if(instance == null) instance = new Terra();
		return instance;
	}
}
