/*
 * listacircolareinteri.h
 *
 *  Created on: Jan 28, 2020
 *      Author: fabio
 */

#ifndef LISTACIRCOLAREINTERI_H_
#define LISTACIRCOLAREINTERI_H_

#include <stdbool.h>

typedef void * lista_ref;

lista_ref make(int max_size);
int prossimo(lista_ref lista);
bool cerca(lista_ref lista, int n);
void aggiungi(lista_ref lista, int n);
char * toString(lista_ref lista);
void elimina(lista_ref lista);

#endif /* LISTACIRCOLAREINTERI_H_ */
