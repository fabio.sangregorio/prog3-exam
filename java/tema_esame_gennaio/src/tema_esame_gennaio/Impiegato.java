package tema_esame_gennaio;

public class Impiegato extends Lavoratore {
	@Override
	public <T> T accept(Visitor<T> v) {
		return v.visit(this);
	}

	public Impiegato() {
		super();
	}
}
