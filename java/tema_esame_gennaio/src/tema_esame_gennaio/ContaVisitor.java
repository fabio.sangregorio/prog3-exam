package tema_esame_gennaio;

public class ContaVisitor implements Visitor<Integer> {

	@Override
	public Integer visit(Manager m) {
		int sum = 1;
		for(Lavoratore i : m.dipendenti) sum += i.accept(this);
		return sum;
	}

	@Override
	public Integer visit(Impiegato i) {
		return 1;
	}

}
