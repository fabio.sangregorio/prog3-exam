public class Persona implements Comparable<Persona> {
	Integer eta;
	
	public Persona(int eta) {
		this.eta = eta;
	}

	@Override
	public int compareTo(Persona o) {
		return this.eta.compareTo(o.eta);
	}
}