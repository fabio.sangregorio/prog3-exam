public class RettangoloV implements Visitable {
	double base;
	double altezza;
	public RettangoloV(double b, double a) {
		base = b;
		altezza = a;
	}
	@Override
	public double accept(Visitor visitor) {
		return visitor.visit(this);
	}
}