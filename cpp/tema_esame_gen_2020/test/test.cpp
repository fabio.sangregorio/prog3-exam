#include <iostream>
using namespace std;

struct A {
	void f() { cout << "A::f" << endl; }
	virtual void vf() { cout << "A::vf" << endl; }
};

struct B : A {
	void f() { cout << "B::f" << endl; }
	void vf() { cout << "B::vf" << endl; }
};

int main() {
	B b;

	A ab = b;
	ab.f();
	ab.vf();

	A* pab = &b;
	pab->f();
	pab->vf();
}
