public class Numero extends Espressione {
	int numero; 
	Numero(int numero) {
		this.numero = numero;
	}
	
	@Override
	public int accept(Visitor visitor) {
		return visitor.visit(this);
	}
}
