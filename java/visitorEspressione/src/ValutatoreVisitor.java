public class ValutatoreVisitor implements Visitor {
	@Override
	public int visit(Somma somma) {
		return somma.a1.accept(this) + somma.a2.accept(this);
	}

	@Override
	public int visit(Prodotto prodotto) {
		return prodotto.a1.accept(this) * prodotto.a2.accept(this);
	}

	@Override
	public int visit(Numero numero) {
		return numero.numero;
	}
}