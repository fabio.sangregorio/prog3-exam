public class visitorNumerica {
	public static void main(String[] args) {
		Espressione a = new Numero(10);
		Espressione b = new Numero(3);
		Espressione c = new Numero(2);
		Espressione d = new Somma(b, c);
		Espressione e = new Prodotto(d, a);
		
		Visitor v = new ValutatoreVisitor();
		System.out.println(e.accept(v));
	}
}