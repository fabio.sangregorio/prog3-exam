public class Cerchio {
	double raggio;
	public Cerchio(double r) {
		raggio = r;
	}
	
	public double getPerimetro() {
		return 2 * Math.PI * raggio;
	}
	
	public double getArea() {
		return Math.PI * raggio * raggio;
	}
}