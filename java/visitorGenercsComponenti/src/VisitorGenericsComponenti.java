

import java.util.ArrayList;
import java.util.stream.Collectors;

public class VisitorGenericsComponenti {
	public static void main(String[] args) {
		Atomico ca1 = new Atomico(5, "CA1");
		Atomico ca2 = new Atomico(10, "CA2");
		Atomico ca3 = new Atomico(15, "CA3");
		Atomico ca4 = new Atomico(20, "CA4");

		ArrayList<Componente> lista1 = new ArrayList<>();
		lista1.add(ca1);
		lista1.add(ca2);
		Composto cc1 = new Composto(lista1);

		ArrayList<Componente> lista2 = new ArrayList<>();
		lista2.add(ca1);
		lista2.add(ca2);
		lista2.add(ca3);
		lista2.add(ca4);
		Composto cc2 = new Composto(lista2);

		PrezzoVisitor prezzo = new PrezzoVisitor();
		NomeVisitor nome = new NomeVisitor();

		System.out.println(ca1.accept(prezzo));
		System.out.println(cc1.accept(prezzo));
		System.out.println(cc2.accept(prezzo));

		System.out.println(ca1.accept(nome));
		System.out.println(cc1.accept(nome));
		System.out.println(cc2.accept(nome));
	}

}