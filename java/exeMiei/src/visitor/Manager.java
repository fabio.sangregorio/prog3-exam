package visitor;

import java.util.ArrayList;

public class Manager extends Dipendente {
	public ArrayList<Dipendente> sottoposti;

	public Manager(ArrayList<Dipendente> sottoposti) {
		super();
		this.sottoposti = sottoposti;
	}

	@Override
	public <T> T accept(Visitor<T> v) {
		return v.visit(this);
	}
}
