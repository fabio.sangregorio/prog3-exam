public class CalcolatorePerimetro {
	public double getPerimetro(Rettangolo r) {
		return 2 * (r.base + r.altezza);
	}
	public double getPerimetro(Quadrato q) {
		return 4 * q.base;
	}
	public double getPerimetro(Cerchio c) {
		return 2 * Math.PI * c.raggio;
	}
}