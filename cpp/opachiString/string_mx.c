#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "string_mx.h"

typedef struct {
	int size;
	int maxsize;
	char * cstr;
} string_mx;

string_ref make(char* str) {
	string_mx *ptr = malloc(sizeof(string_mx));
	ptr->size = strlen(str) + 1;
	ptr->maxsize = 50;
	ptr->cstr = (char *)malloc(ptr->size*sizeof(char));
	strcpy(ptr->cstr, str);
	return (string_ref)ptr;
}

void strccpy_mx(string_ref * s1, string_ref * s2) {
	strncpy(((string_mx*)s1)->cstr, ((string_mx*)s2)->cstr, ((string_mx*)s1)->size);
}
