

object contaPari {
  def pari(n: Int): Int = if(n % 2 == 0) 1 else 0
  
  def contaPariFor(lista: List[Int]): Int = {
    var sum = 0
    for(i <- lista) sum += pari(i)
    sum
  }
  
  def contaPariForeach(lista: List[Int]): Int = {
    var sum = 0
    lista.foreach(sum += pari(_))
    sum
  }
  
  def contaPariRic(lista: List[Int]): Int = {
    if(lista.size == 1) pari(lista.head)
    else pari(lista.head) + contaPariRic(lista.tail)
  }
  
  def contaPariFold(lista: List[Int]): Int = lista.foldLeft(0)(pari(_) + pari(_))
  
  def contaPariFilter(lista: List[Int]): Int = lista.filter(pari(_) == 1).size
  
  def contaP(p: Int => Boolean) = (lista: List[Int]) => {
    def contaPFilter(lista: List[Int]): Int = lista.filter(p(_)).size
    contaPFilter(lista)
  }
  
  def contaPari2(lista: List[Int]): Int = {
    contaP((a: Int) => a % 2 == 0)(lista)
  }
  
  def main(args: Array[String]) {
    val lista = List.range(0, 10)
    println(contaPariFor(lista))
    println(contaPariForeach(lista))
    println(contaPariRic(lista))
    println(contaP((a: Int) => a % 2 == 0)(lista))
    def pariB(n: Int): Boolean = n % 2 == 0
    println(contaP(pariB)(lista))
    println(contaPari2(lista))
  }
}