public class Rettangolo {
	double base;
	double altezza;
	public Rettangolo(double b, double a) {
		base = b;
		altezza = a;
	}
	
	public double getArea() {
		return base * altezza;
	}
	
	public double getPerimetro() {
		return 2 * (base + altezza);
	}
}
