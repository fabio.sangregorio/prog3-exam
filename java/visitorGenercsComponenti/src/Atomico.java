public class Atomico extends Componente {
	int prezzo;
	String nome;
	
	Atomico(int prezzo, String nome) {
		this.prezzo = prezzo;
		this.nome = nome;
	}

	@Override
	public <T> T accept(Visitor<T> v) {
		return v.visit(this);
	}
}