#include <stdio.h>
#include <stdlib.h>

void del1Int(int **p) {
	*p = *p + 1;
}

int main(int argc, char **argv) {
	int a[] = { 1, 2, 3 };
	int *p = a;
	printf("%d\n", *p);
	del1Int(&p);
	printf("%d\n", *p);
}
