public class CerchioV implements Visitable {
	double raggio;
	public CerchioV(double r) {
		raggio = r;
	}
	@Override
	public double accept(Visitor visitor) {
		return visitor.visit(this);
	}
}