public class SequenzaOrdinabile<T extends Comparable<T>> {
	 public Object[] sequenza;
	
	public SequenzaOrdinabile() {
		sequenza = new Object[0];
	}
	
	public void insert(T item) {
		Object[] tmp = new Object[sequenza.length + 1];
		for(int i = 0; i < sequenza.length; i++) tmp[i] = sequenza[i];
		tmp[sequenza.length] = item;
		sequenza = tmp;
	}
	
	public void sort() {
		for(int i = 0; i < sequenza.length; i++) {
			for(int j = 0; j < sequenza.length - 1; j++) {
				T first = (T)sequenza[i];
				T second = (T)sequenza[j];
				if(first.compareTo(second) < 0) {
					sequenza[i] = second;
					sequenza[j] = first;
				}
			}
		}
	}
	
	public String toString() {
		String s = sequenza.length + " items: ";
		for(int i = 0; i < sequenza.length; i++) s += sequenza[i] + " ";
		return s;
	}
	
	public static void main(String[] args) {
		SequenzaOrdinabile<String> s = new SequenzaOrdinabile<String>();		
		s.insert("bbb");
		s.insert("ccc");
		s.insert("eee");
		s.insert("ddd");
		s.insert("aaa");
		
		System.out.println(s.toString());
		System.out.println(SequenzaUtils.mean(s));
		s.sort();
		System.out.println(s.toString());
		System.out.println(SequenzaUtils.mean(s));
		
		// SequenzaOrdinabile<Person> = new SequenzaOrdinabile<Student>();
		// Non posso usarla perche' SequenzaOrdinabile<Student> non eredita da SequenzaOrdinabile<Person>
	}
}

class SequenzaUtils {
	static <T extends Comparable<T>> Object mean(SequenzaOrdinabile<T> seq) {
		return seq.sequenza[(int) Math.floor((double)(seq.sequenza.length / 2))];
	}
}