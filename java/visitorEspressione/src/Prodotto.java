public class Prodotto extends Espressione {
	Espressione a1;
	Espressione a2;
	
	Prodotto(Espressione a1, Espressione a2) {
		this.a1 = a1;
		this.a2 = a2;
	}

	@Override
	public int accept(Visitor visitor) {
		return visitor.visit(this);
	}
}