#include <stdio.h>
#include <stdlib.h>

int getNumero() {
	printf("Inserisci un numero < 10 per salvare il pianeta, un numero > 10 per lanciare la bomba nucleare:\n...\n");
	return 5;
}

int* seiSicuro() {
	printf("Sei sicuro? Reinserisci un numero > 10 per lanciare la bomba nucleare:\n...\n");
	int x = 5;
	int* p = &x;
	printf("Numero inserito: %d\n", x);
	return p;
}

void lanciaBombaNucleare() {
	printf("Bomba nucleare lanciata ");
}

int main(void) {
	int *numero = malloc(sizeof(int));
	*numero = getNumero();
	printf("Numero inserito: %d\n", *numero);
	free(numero);
	if (*numero > 10) {
		printf("Hai inserito %d, ", *numero);
		int *numeroSicuro;
		numeroSicuro = seiSicuro();
		fflush(stdin);
		if (*numeroSicuro > 10) {
			lanciaBombaNucleare();
			printf("perche hai inserito il numero %d\n", *numero);
		}
	}

	return 0;
}
