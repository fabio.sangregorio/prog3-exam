#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char * togliNumeri(char * str) {
	char * strNoNum = malloc(sizeof(char) * (strlen(str) + 1));
	int n = 0;
	for(int i = 0; i < strlen(str); i++) {
		if(!isdigit(str[i])) {
			strNoNum[n] = str[i];
			n++;
		}
	}
	strNoNum[n] = '\0';

	char * res = malloc(sizeof(char) * n);
	strncpy(res, strNoNum, n);
	res[n] = '\0';
	free(strNoNum);
	return res;
}

char * togliNumeriRec(char * str, char * res) {
	if(strlen(str) == 1) {					// se sono alla fine
		if(isdigit(str[0])) return res;		// se non e' lettera ritorno il res (che sara' vuoto)
		else return strncpy(res, str, 2);	// se e' lettera ritorno l'ultima lettera + \0
	}
	strncpy(res, togliNumeriRec(str + 1, res), strlen(res)); 	// se non sono alla fine copio il risultato (che parte dalla fine della stringa) in res
	if(!isdigit(str[0])) {										// se il char attuale non e' un numero
		char * tmp = malloc(sizeof(char) * strlen(res) + 2); 	// creo una variabile tmp
		strncpy(tmp, str, 1);									// ci copio dentro la lettera attuale
		strncat(tmp, res, strlen(res) + 1);						// ci concateno res (la parte destra della stringa)
		strncpy(res, tmp, strlen(res) + 1);						// copio tmp in res e libero tmp
		free(tmp);
	}
	return res;
}

char * togliNumeriRecInit(char * str) {
	char * res = malloc(sizeof(char) * (strlen(str) + 1));
	res[0] = '\0';
	res = togliNumeriRec(str, res);
	return res;
}

char * togliNumeriTail(char * str, char * res) {
	if(!isdigit(str[0])) strncat(res, str, 1);
	if(str[0] == '\0') return res;
	return togliNumeriTail(str + 1, res);
}

char * togliNumeriTailInit(char * str) {
	char * res = malloc(sizeof(char) * (strlen(str) + 1));
	res[0] = '\0';
	res = togliNumeriTail(str, res);
	return res;
}

int main(int argc, char **argv) {
	char * str = "st1caZz1";
	printf("%s\n", togliNumeri(str));
	printf("%s\n", togliNumeriRecInit(str));
	printf("%s\n", togliNumeriTailInit(str));
}
