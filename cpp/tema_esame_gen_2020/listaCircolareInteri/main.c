/*
 * main.c
 *
 *  Created on: Jan 28, 2020
 *      Author: fabio
 */
#include <stdio.h>
#include <stdlib.h>
#include "listacircolareinteri.h"

int main(int argc, char **argv) {
	lista_ref list = make(10);
	for(int i = 1; i <= 10; i++) aggiungi(list, i);
	printf("%s\n", toString(list));
	printf("%d\n", prossimo(list));
	printf("%d\n", prossimo(list));
	printf("%d\n", prossimo(list));
	printf("%s\n", toString(list));
	aggiungi(list, 11);
	printf("%d\n", cerca(list, 3));
	printf("%d\n", cerca(list, 11));
	elimina(list);
}
