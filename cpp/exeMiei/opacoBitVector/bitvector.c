#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "bitvector.h"

typedef struct {
	unsigned short * array;
	int array_size;
} vector;

int SHORT_LEN = sizeof(short) * 8; // in uno short ci stanno 16 bit

vector_ref make(int size) {
	vector* v = malloc(sizeof(vector));

	v->array_size = ceil((double)size/SHORT_LEN);
	v->array = malloc(v->array_size);
	for(int i = 0; i < v->array_size; i++) {
		v->array[i] = 5;
	}
	return (vector_ref)v;
}

void edit(vector_ref v, int boolean, int index) {
	if(boolean < 0 && boolean > 1) {
		printf("l'input deve essere 0 o 1\n");
		return;
	}
	if(index > ((vector*)v)->array_size * SHORT_LEN) {
		printf("index out of bound\n");
		return;
	}

	int i = index/SHORT_LEN;
	unsigned short curr = ((vector*)v)->array[i];
	unsigned short * tmp = malloc(sizeof(short) * ((vector*)v)->array_size * SHORT_LEN);

	for(int j = 0; j != SHORT_LEN; ++j) {
		tmp[j] = curr % 2;
		curr /= 2;
	}
	tmp[index] = boolean;
	curr = 0;
	for(int j = 0; j != SHORT_LEN; ++j)
		curr += tmp[j]*pow(2, j);
	((vector*)v)->array[i] = curr;
}

char * toString(vector_ref v) {
	int size = (((vector*)v)->array_size * SHORT_LEN) * sizeof(char) + 2;
	char * str = malloc(size);
	int stri = 0;
	for(int i = 0; i < ((vector*)v)->array_size; i++) {
		unsigned short curr = ((vector*)v)->array[i];
		for(int j = 0; j != SHORT_LEN; ++j) {
			stri += sprintf(str + stri, "%d", curr % 2);
			curr /= 2;
		}
	}
	str[size - 1] = '\0';
	return str;
}

int main(int argc, char **argv) {
	vector_ref v = make(16);
	edit(v, 1, 3);
	printf(toString(v));
}
