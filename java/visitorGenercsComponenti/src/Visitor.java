public interface Visitor<T> {
	abstract T visit(Atomico atomico);
	abstract T visit(Composto composto);
}