import java.util.ArrayList;

public abstract class Componente implements Visitable {
	static int sommaCosto(ArrayList<Componente> componenti) {
		int sum = 0;
		PrezzoVisitor v = new PrezzoVisitor();
		for(Componente c: componenti) sum += c.accept(v);
		return sum;
	}
}