package visitor;

public interface Visitor<T> {
	public T visit(Manager m);
	public T visit(Sottoposto s);
}
