package tema_esame_gennaio;

import java.util.ArrayList;

public class main {
	public static double sommaStipendi(ArrayList<Lavoratore> l) {
		StipendioVisitor sv = new StipendioVisitor();
		double sum = 0;
		for(Lavoratore i: l) {
			sum += i.accept(sv);
		}
		return sum;
	}
	
	public static void main(String[] args) {
		Manager m1 = new Manager(new ArrayList<Lavoratore>() {
			{
				add(new Impiegato());
				add(new Impiegato());
			}
		});
		Manager m2 = new Manager(new ArrayList<Lavoratore>() {
			{
				add(new Impiegato());
				add(new Impiegato());
			}
		});
		Manager m3 = new Manager(new ArrayList<Lavoratore>() {
			{
				add(new Impiegato());
				add(new Impiegato());
				add(m2);
				add(m1);
			}
		});
		
		ContaVisitor cv = new ContaVisitor();
		StipendioVisitor sv = new StipendioVisitor();
		System.out.println(cv.visit(m3));
		System.out.println(sv.visit(m3));
		System.out.println(sommaStipendi(new ArrayList<Lavoratore>() {
			{
				add(m3);
				add(m1);
			}
		}));
		
;	}
}
