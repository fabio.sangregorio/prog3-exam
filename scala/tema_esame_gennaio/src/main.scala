object main {
  def main(args: Array[String]) {
    var str = "ciao"
    println(hasNumbers(str))
    println(hasNumbersRec(str))
    println(hasNumbersTail(str))
   
    var list = List("ciao", "c1ao", "test", "te5t", "101")
    println(cscn_for(list))
    println(cscn_foreach(list))
    println(cscn_ric(list))
    println(cscn_ric_tail(list))
    println(cscn_filter(list))
    println(cscn_map(list))
    println(cscn_fold(list))
    
    println(csc((str) => str.contains("o"))(list))
    println(csc(hasNumbersRec)(list))
  }
  
  def toInt(b: Boolean): Int = {
    if(b) 1 else 0
  }
  
  def hasNumbers(str: String): Boolean = {
    var sum = 0
    str.chars().forEach(c => if(Character.isDigit(c)) sum += 1)
    sum > 0
  }
  
  def hasNumbersRec(str: String): Boolean = {
    if(str.length() == 1) Character.isDigit(str.head)
    else hasNumbersRec(str.head.toString()) || hasNumbers(str.tail)
  }
  
  def hasNumbersTail(str: String): Boolean = {
    if(Character.isDigit(str.head)) true
    else if(str.length() == 1) false
    else hasNumbersTail(str.tail)
  }
  
  def cscn_for(list: List[String]): Int = {
    var sum = 0
    for(str <- list) if(hasNumbersTail(str)) sum += 1
    sum
  }
  
  def cscn_foreach(list: List[String]): Int = {
    var sum = 0
    list.foreach(str => if(hasNumbersTail(str)) sum += 1)
    sum
  }
  
  def cscn_ric(list: List[String]): Int = {
    if(list.length == 1) toInt(hasNumbersTail(list.head))
    else cscn_ric(List(list.head)) + cscn_ric(list.tail)
  }
  
  def cscn_ric_tail(list: List[String]): Int = {
    def inner(list: List[String], sum: Int): Int = {
      if(list.length == 1) sum + toInt(hasNumbersTail(list.head))
      else inner(list.tail, sum + toInt(hasNumbersTail(list.head)))
    }
    
    inner(list, 0)
  }
  
  def cscn_filter(list: List[String]): Int = {
    list.filter(hasNumbersTail(_)).size
  }
  
  def cscn_map(list: List[String]): Int = {
    list.map(hasNumbersTail(_)).count(_ == true)
  }
  
  def cscn_fold(list: List[String]): Int = {
    list.foldLeft(0)((sum, item) => sum + toInt(hasNumbersTail(item)))
  }
  
  def csc(f: String => Boolean)(list: List[String]) = {
    list.filter(f(_)).size
  }
}