public class Coppia<K extends Comparable<K>, V> implements Comparable<Coppia<K, V>> {
	K key;
	V value;

	Coppia(K key, V value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public int compareTo(Coppia<K, V> o) {
		return this.key.compareTo(o.key);
	}
}