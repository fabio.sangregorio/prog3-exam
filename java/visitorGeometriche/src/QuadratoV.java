public class QuadratoV implements Visitable {
	double base;
	public QuadratoV(double b) {
		base = b;
	}
	@Override
	public double accept(Visitor visitor) {
		return visitor.visit(this);
	}
}