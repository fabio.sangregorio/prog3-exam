package tema_esame_gennaio;

import java.util.ArrayList;

public class Manager extends Lavoratore {
	public ArrayList<Lavoratore> dipendenti;

	public Manager(ArrayList<Lavoratore> dipendenti) {
		super();
		this.dipendenti = dipendenti;
	}

	@Override
	public <T> T accept(Visitor<T> v) {
		return v.visit(this);
	}
}
