public interface Visitor {
	double visit(RettangoloV rettangoloV);
	double visit(CerchioV cerchioV);
	double visit(QuadratoV quadratoV);
}