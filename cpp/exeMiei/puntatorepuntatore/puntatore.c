#include <stdlib.h>
#include <stdio.h>

void del1Int(int ** buff) {
	*buff = *buff + 1;
}

int main(int argc, char **argv) {
	int a[] = {1, 2, 3};
	int * p = a;
	printf("%d\n", *p);
	del1Int(&p);
	printf("%d\n", *p);
}
