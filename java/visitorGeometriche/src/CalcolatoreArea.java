public class CalcolatoreArea {
	public double getArea(Rettangolo r) {
		return r.base * r.altezza;
	}
	
	public double getArea(Quadrato q) {
		return q.base * q.base;
	}
	
	public double getArea(Cerchio c) {
		return Math.PI * c.raggio * c.raggio;
	}
}