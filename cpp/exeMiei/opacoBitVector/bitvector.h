typedef void * vector_ref;
vector_ref make(int len);
void edit(vector_ref v, int boolean, int index);
vector_ref and(vector_ref v1, vector_ref v2);
vector_ref or(vector_ref v1, vector_ref v2);
vector_ref not(vector_ref v);
char * toString(vector_ref v);
void delete(vector_ref v);
