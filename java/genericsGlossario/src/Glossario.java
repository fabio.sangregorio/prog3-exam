import java.util.ArrayList;
import java.util.stream.Collectors;

public class Glossario<K extends Comparable<K>, V> {
	ArrayList<Coppia<K, V>> array;

	Glossario() {
		array = new ArrayList<Coppia<K, V>>();
	}

	public void insertInOrdine(Coppia<K, V> c) {
		for(int i = 0; i < array.size(); i++) {
			if(array.get(i).key.compareTo(c.key) >= 0) {
				array.add(i, c);
				return;
			}
			if(i == array.size() - 1) {
				array.add(c);
				return;
			}
		}
		if(array.size() == 0) array.add(c);
	}
	
	public <K> V find(K key) {
		ArrayList<K> keys = (ArrayList<K>) array.stream().map(c->c.key).collect(Collectors.toList());
		int index = keys.indexOf(key);
		return index < 0 ? null : (V)array.get(index).value;
	}
	
	public String toString() {
		return array.size() + ":\n" + array.stream()
				.map(c->c.key.toString() + " - " + c.value.toString()).collect(Collectors.joining("\n"));
	}
	
	public static void main(String[] args) {
		Glossario<String, String> glossario = new Glossario<String, String>();
		glossario.insertInOrdine(new Coppia<String, String>("E", "e"));
		glossario.insertInOrdine(new Coppia<String, String>("F", "f"));
		glossario.insertInOrdine(new Coppia<String, String>("G", "g"));
		glossario.insertInOrdine(new Coppia<String, String>("C", "c"));
		glossario.insertInOrdine(new Coppia<String, String>("B", "b"));
		glossario.insertInOrdine(new Coppia<String, String>("A", "a"));
		glossario.insertInOrdine(new Coppia<String, String>("D", "d"));
		System.out.println(glossario.toString());
		System.out.println(glossario.find("C"));
	}
}


