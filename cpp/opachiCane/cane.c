#include "cane.h"

#include <stdlib.h>
#include <stdio.h>

typedef struct {
	char * nome;
} cane_struct;

cane_ref make_cane(char* nome) {
	cane_struct *ptr = malloc(sizeof(cane_struct));
	ptr->nome = nome;
	return (cane_ref)ptr;
}

void abbaia(cane_ref c) {
	printf(((cane_struct*)c)->nome);
}


