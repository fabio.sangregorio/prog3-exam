public class Quadrato {
	double base;
	public Quadrato(double b) {
		base = b;
	}
	
	public double getArea() {
		return base * base;
	}
	
	public double getPerimetro() {
		return 4 * base;
	}
}