typedef void * string_ref;

string_ref make(char*);
void strccpy_mx(string_ref, string_ref);
char* tostring(string_ref);
void delete_mx(string_ref);
