#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

bool RESULT;

bool isPalindroma(char * str, int size) {
	for(int i = 0; i < size/2; i++) if(str[i] != str[size - i - 1]) return false;
	return true;
}

bool isPalindromaRic(char * str, int size) {
	if(size <= 1) return true;
	bool same = str[0] == str[size - 1];
	return same && isPalindromaRic(str + 1, size - 2);
}

bool isPalindromaTail(char * str, int size) {
	if(size <= 1) return true;
	bool same = str[0] == str[size - 1];
	if(!same) return false;
	return isPalindromaTail(str + 1, size - 2);
}

int main(int argc, char **argv) {
	char * str = "anna";
	RESULT = isPalindromaTail(str, strlen(str));
	printf("%d\n", RESULT);
	printf("%d\n", isPalindromaRic(str, strlen(str)));
	printf("%d\n", isPalindroma(str, strlen(str)));
}
