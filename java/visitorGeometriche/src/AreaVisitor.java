public class AreaVisitor implements Visitor {
	@Override
	public double visit(RettangoloV rettangoloV) {
		return rettangoloV.base * rettangoloV.altezza;
	}

	@Override
	public double visit(CerchioV cerchioV) {
		return Math.PI * cerchioV.raggio * cerchioV.raggio;
	}

	@Override
	public double visit(QuadratoV quadratoV) {
		return quadratoV.base * quadratoV.base;
	}
}