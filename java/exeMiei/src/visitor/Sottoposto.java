package visitor;

public class Sottoposto extends Dipendente {

	public Sottoposto() {
		super();
	}

	@Override
	public <T> T accept(Visitor<T> v) {
		return v.visit(this);
	}
}
