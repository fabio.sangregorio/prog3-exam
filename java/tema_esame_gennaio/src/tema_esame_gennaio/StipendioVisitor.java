package tema_esame_gennaio;

public class StipendioVisitor implements Visitor<Double> {
	ContaVisitor cv = new ContaVisitor();

	@Override
	public Double visit(Manager m) {
		return 2.5 * m.accept(cv);
	}

	@Override
	public Double visit(Impiegato i) {
		return (double) 10;
	}
	
}
