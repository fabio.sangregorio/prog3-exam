public class visitorGeometriche {
	public static void main(String[] args) {
		Rettangolo r = new Rettangolo(1, 2);
		Quadrato q = new Quadrato(3);
		Cerchio c = new Cerchio(4);
		
		System.out.println(r.getArea());
		System.out.println(q.getArea());
		System.out.println(c.getArea());
		System.out.println(r.getPerimetro());
		System.out.println(q.getPerimetro());
		System.out.println(c.getPerimetro());
		
		CalcolatoreArea ca = new CalcolatoreArea();
		CalcolatorePerimetro cp = new CalcolatorePerimetro();
		
		System.out.println(ca.getArea(r));
		System.out.println(ca.getArea(q));
		System.out.println(ca.getArea(c));
		System.out.println(cp.getPerimetro(r));
		System.out.println(cp.getPerimetro(q));
		System.out.println(cp.getPerimetro(c));
		
		RettangoloV rv = new RettangoloV(1, 2);
		QuadratoV qv = new QuadratoV(3);
		CerchioV cv = new CerchioV(4);

		AreaVisitor areaV = new AreaVisitor();
		PerimetroVisitor perV = new PerimetroVisitor();
		
		System.out.println(rv.accept(areaV));
		System.out.println(qv.accept(areaV));
		System.out.println(cv.accept(areaV));
		System.out.println(rv.accept(perV));
		System.out.println(qv.accept(perV));
		System.out.println(cv.accept(perV));
		
	}
}