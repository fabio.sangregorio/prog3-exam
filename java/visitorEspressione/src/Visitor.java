public interface Visitor {
	int visit(Somma somma);
	int visit(Prodotto prodotto);
	int visit(Numero numero);
}
