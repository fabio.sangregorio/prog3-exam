import java.util.ArrayList;

public class Composto extends Componente {
	ArrayList<Componente> componenti;
	
	Composto(ArrayList<Componente> componenti) {
		this.componenti = componenti;
	}
	
	@Override
	public <T> T accept(Visitor<T> v) {
		return v.visit(this);
	}
}